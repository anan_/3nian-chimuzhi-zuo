
#include "view.h"


void View::CalculateView(const VECTOR3& eye, const VECTOR3& focus, const VECTOR3& upVector)
{
	DirectX::XMVECTOR E, F, U;
	E = DirectX::XMLoadFloat3(&eye);
	F = DirectX::XMLoadFloat3(&focus);
	U = DirectX::XMLoadFloat3(&upVector);

	DirectX::XMMATRIX V;
	V = DirectX::XMMatrixLookAtLH(E, F, U);
	DirectX::XMStoreFloat4x4(&view, V);
}

void View::CalculateView(const FLOAT4X4& world)
{
	DirectX::XMMATRIX V;
	V = DirectX::XMMatrixInverse(NULL, DirectX::XMLoadFloat4x4(&world));
	DirectX::XMStoreFloat4x4(&view, V);
}

