#pragma once

#include <string>
#include <vector>
#include "vector.h"
#include <wrl.h>

#undef max
#undef min

#include<cereal/cereal.hpp>
#include<cereal/archives/binary.hpp>
#include<cereal/types/memory.hpp>
#include<cereal/types/vector.hpp>
using Microsoft::WRL::ComPtr;

template<class Archive>
void serialize(Archive& archive, VECTOR2& vector)
{
	archive(vector.x, vector.y);
}

template<class Archive>
void serialize(Archive& archive, VECTOR3& vector)
{
	archive(vector.x, vector.y, vector.z);
}

template<class Archive>
void serialize(Archive& archive, UVECTOR4& vector)
{
	archive(vector.x, vector.y, vector.z, vector.w);
}

template<class Archive>
void serialize(Archive& archive, VECTOR4& vector)
{
	archive(vector.x, vector.y, vector.z, vector.w);
}

template<class Archive>
void serialize(Archive& archive, FLOAT4X4& float4x4)
{
	archive(
		float4x4._11, float4x4._12, float4x4._13, float4x4._14,
		float4x4._21, float4x4._22, float4x4._23, float4x4._24,
		float4x4._31, float4x4._32, float4x4._33, float4x4._34,
		float4x4._41, float4x4._42, float4x4._43, float4x4._44);
}


struct ModelData
{

	ModelData() = default;
	~ModelData() = default;
	ModelData(const char* filename, bool flipping_v_coodinates = false, std::string extension = "bin");
	struct Node
	{
		std::string name;
		int parentIndex;
		VECTOR3 scale;
		VECTOR4 rotate;
		VECTOR3 translate;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				name,
				parentIndex,
				scale.x, scale.y, scale.z,
				rotate.x, rotate.y, rotate.z, rotate.w,
				translate.x, translate.y, translate.z
			);
		}
	};

	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
		VECTOR2 texcoord;
		VECTOR4 boneWeight;
		UINT4 boneIndex;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				position.x, position.y, position.z,
				normal.x, normal.y, normal.z,
				texcoord.x, texcoord.y,
				boneWeight.x, boneWeight.y, boneWeight.z, boneWeight.w,
				boneIndex.x, boneIndex.y, boneIndex.z, boneIndex.w
			);
		}
	};

	struct Subset
	{
		int materialIndex;
		int startIndex;
		int indexCount;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				materialIndex,
				startIndex,
				indexCount
			);
		}
	};

	struct Face
	{
		VECTOR3 position[3];
		int materialIndex;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				position[0].x, position[0].y, position[0].z,
				position[1].x, position[1].y, position[1].z,
				position[2].x, position[2].y, position[2].z,
				materialIndex
			);
		}
	};

	struct Mesh
	{
		std::vector<Vertex> vertices;
		std::vector<int> indices;
		std::vector<Subset> subsets;

		int nodeIndex;

		std::vector<int> nodeIndices;
		FLOAT4X4 globalTransform = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		std::vector<FLOAT4X4> inverseTransforms;

		std::vector<Face> faces;

		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				vertices,
				indices,
				subsets,
				nodeIndex,
				nodeIndices,
				inverseTransforms,
				faces
			);
		}
	};

	struct Material
	{
		VECTOR4 color;
		std::string textureFileName;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				color.x, color.y, color.z, color.w,
				textureFileName
			);
		}
	};

	struct NodeKeyData
	{
		VECTOR3 scale;
		VECTOR4 rotate;
		VECTOR3 translate;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				scale.x, scale.y, scale.z,
				rotate.x, rotate.y, rotate.z, rotate.w,
				translate.x, translate.y, translate.z
			);
		}
	};

	struct KeyFrame
	{
		float seconds;
		std::vector<NodeKeyData> nodeKeys;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				seconds,
				nodeKeys
			);
		}
	};
	struct Animation
	{
		float secondsLength;
		std::vector<KeyFrame> keyFrames;
		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(
				secondsLength,
				keyFrames
			);
		}
	};

	std::vector<Node> nodes;

	std::vector<Mesh> meshes;
	std::vector<Material> materials;

	std::vector<Animation> animations;

	char dirName[256];

	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(
			nodes,
			meshes,
			materials,
			animations,
			dirName
		);
	}

};