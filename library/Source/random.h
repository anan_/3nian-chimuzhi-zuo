#pragma once
#include <cstdlib>
inline int GetIntRandom(int min, int max)
{
	int sub = max - min;
	return min + (int)(rand()*(sub + 1.0) / (1.0 + RAND_MAX));
}

inline float GetFloatRandom(float min, float max)
{
	float sub = max - min;
	return min + static_cast<float>((rand()*(sub + 1.0) / (1.0 + RAND_MAX)));
}