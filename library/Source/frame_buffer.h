#pragma once

#include <d3d11.h>
#include <wrl.h>
using Microsoft::WRL::ComPtr;
#include <assert.h>
#include <memory>

#include "create_DX11.h"

class FrameBuffer
{
public:

	FrameBuffer(ID3D11Device* device, int width, int height, bool enableMsaa = false, int subsamples = 1,
		DXGI_FORMAT renderTargetTexture2dFormat = DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_FORMAT depthStencilTexture2dFormat = DXGI_FORMAT_R24G8_TYPELESS,
		bool needRenderTargetShaderResourceView = true, bool needDepthStencilShaderResourceView = true,
		bool generateMips = false);
	//FrameBuffer(ID3D11Device* device, ID3D11RenderTargetView* sharedRenderTargetView,
	//	DXGI_FORMAT depthStencilTexture2dFormat = DXGI_FORMAT_R24G8_TYPELESS, bool needDepthStencilShaderResourceView = true);
	//FrameBuffer(ID3D11Device* device, ID3D11DepthStencilView* sharedDepthStencilView,
	//	DXGI_FORMAT renderTargetTexture2dFormat = DXGI_FORMAT_R8G8B8A8_UNORM, bool needRenderTargetShaderResourceView = true);
	//FrameBuffer(ID3D11Device* device, ID3D11RenderTargetView* sharedRenderTargetView,
	//	ID3D11DepthStencilView* sharedDepth_stencilView);

	virtual ~FrameBuffer() = default;

	void Clear(ID3D11DeviceContext* context,
		float r = 0.f, float g = 0.f, float b = 0.f, float a = 1.f,
		unsigned int clearFlags = D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		float depth = 1, unsigned char stencil = 0)
	{
		float color[4] = { r, g, b, a };
		if (renderTargetView)
		{
			context->ClearRenderTargetView(renderTargetView.Get(), color);
		}
		if (depthStencilView)
		{
			context->ClearDepthStencilView(depthStencilView.Get(), clearFlags, depth, stencil);
		}
	}

	void ClearRenderRTargetView(ID3D11DeviceContext* context, 
		float r = 0.f, float g = 0.f, float b = 0.f, float a = 1.f)
	{
		float color[4] = { r, g, b, a };
		if (renderTargetView)
		{
			context->ClearRenderTargetView(renderTargetView.Get(), color);
		}
	}
	void ClearDepthStencilView(ID3D11DeviceContext* context,
		unsigned int clearFlags = D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		float depth = 1, unsigned char stencil = 0)
	{
		if (depthStencilView)
		{
			context->ClearDepthStencilView(depthStencilView.Get(), clearFlags, depth, stencil);
		}
	}

	void Activate(ID3D11DeviceContext* context)
	{
		numberOfStoredViewports = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE;
		context->RSGetViewports(&numberOfStoredViewports, defaultViewports);
		context->RSSetViewports(1, &viewport);

		context->OMGetRenderTargets(1, defaultRenderTargetView.ReleaseAndGetAddressOf(), defaultDepthStencilView.ReleaseAndGetAddressOf());
		context->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
	}

	void ActivateRenderRTargetView(ID3D11DeviceContext* context)
	{
		numberOfStoredViewports = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE;
		context->RSGetViewports(&numberOfStoredViewports, defaultViewports);
		context->RSSetViewports(1, &viewport);

		context->OMGetRenderTargets(1, defaultRenderTargetView.ReleaseAndGetAddressOf(), defaultDepthStencilView.ReleaseAndGetAddressOf());
		context->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), 0);
	}
	void ActivateDepthStencilView(ID3D11DeviceContext* context)
	{
		numberOfStoredViewports = D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE;
		context->RSGetViewports(&numberOfStoredViewports, defaultViewports);
		context->RSSetViewports(1, &viewport);

		context->OMGetRenderTargets(1, defaultRenderTargetView.ReleaseAndGetAddressOf(), defaultDepthStencilView.ReleaseAndGetAddressOf());
		ID3D11RenderTargetView* nullRenderTargets = 0;
		context->OMSetRenderTargets(1, &nullRenderTargets, depthStencilView.Get());
	}

	void DeActivate(ID3D11DeviceContext* context)
	{
		context->RSSetViewports(numberOfStoredViewports, defaultViewports);
		context->OMSetRenderTargets(1, defaultRenderTargetView.GetAddressOf(), defaultDepthStencilView.Get());
	}

private:
	ComPtr<ID3D11RenderTargetView> renderTargetView;
	ComPtr<ID3D11DepthStencilView> depthStencilView;

	ComPtr<ID3D11ShaderResourceView> renderTargetShaderResourceView;
	ComPtr<ID3D11ShaderResourceView> depthStencilShaderResourceView;

	D3D11_VIEWPORT viewport;

	D3D11_VIEWPORT defaultViewports[D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE];
	unsigned int numberOfStoredViewports;

	ComPtr<ID3D11RenderTargetView> defaultRenderTargetView;
	ComPtr<ID3D11DepthStencilView> defaultDepthStencilView;
};