#pragma once

#include "vector.h"

class Projection
{
public:
	Projection() {}
	~Projection() {}

	void CalculateOrthographic(float w, float h, float znear, float zfar) //	���s���e�s��ݒ�֐�
	{
		DirectX::XMMATRIX P = DirectX::XMMatrixOrthographicLH(w, h, znear, zfar);
		DirectX::XMStoreFloat4x4(&projection, P);
	}
	void CalculatePerspective(float fovDegree, float aspect, float zNear = 0.1f, float zFar = 1000.f) // �������e�s��ݒ�֐�
	{
		DirectX::XMMATRIX P = DirectX::XMMatrixPerspectiveFovLH(ToRadian(fovDegree), aspect, zNear, zFar);
		DirectX::XMStoreFloat4x4(&projection, P);
	}

	const FLOAT4X4& GetProjection()
	{
		return projection;
	}

private:
	FLOAT4X4 projection;
};