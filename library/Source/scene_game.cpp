
#include "scene_game.h"
#include "create_DX11.h"
#include "fadeout.h"
#include "load_model.h"

#include "input.h"
#include "hitbox_render.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void SceneGame::Init(ID3D11Device* device)
{
	renderer = std::make_unique<ModelRenderer>(device);

	Camera& camera = Camera::GetInstance();

	LightInit(device);
	cameraControl = std::make_unique<CameraControl>();

	std::shared_ptr<GeometricPrimitive> sphere = std::make_shared<GeometricSphere>(device);
	std::shared_ptr<GeometricPrimitive> cube = std::make_shared<GeometricCube>(device);

	obj1 = std::make_unique<GeometricObj>();
	//obj2 = std::make_unique<GeometricObj>(device, sphere);
	obj1->SetGeometric(cube);
	obj1->SetPosition(VECTOR3(0, -1.f, 0));
	obj1->SetScale(VECTOR3(30.f, 1.f, 30.f));
	//obj2->SetPosition(VECTOR3(-2, 0, 0));

	//HitBoxRender::Create(device);

	LoadModel::Load(device, "Data/fbx/danbo_fbx/danbo_fly.fbx", "FlyingDNB");
	player = std::make_unique<Player>(device, LoadModel::GetModelResource("FlyingDNB"));
}

void SceneGame::Release()
{
	//HitBoxRender::Destory();
	Camera::Destory();
}

void SceneGame::Update(ID3D11Device* device, float elapsedTime)
{
	cameraControl->Activate(elapsedTime);

	obj1->Update();
	static float x = 2.f;
	static Key A('A'), D('D');
	//if (A.state(TRIGGER_MODE::NONE))
	//{
	//	x += 0.2f;
	//}
	//else if (D.state(TRIGGER_MODE::NONE))
	//{
	//	x -= 0.2f;
	//}

	s1.position = VECTOR3(x, 0, 0);
	s1.radius = 0.5f;
	s1.scale = 1.f;
	s2.position = VECTOR3(-2, 0, 0);
	s2.radius = 0.5f;
	s2.scale = 1.f;

	//HitBoxRender::GetInstance().SetSphere(device, s1);
	//HitBoxRender::GetInstance().SetSphere(device, s2);

	//HitBoxRender::GetInstance().Update();
	if (Collision::HitSphere(s2, s1))
	{
		int a = 0;
	}

	player->Update(elapsedTime);
}

void SceneGame::Render(ID3D11DeviceContext* context, float elapsedTime)
{
	SetRender::SetBlender(BLEND_LAVEL::BS_ALPHA);
	SetRender::SetDepthStencilState(DEPTH_LAVEL::DS_TRUE);
	SetRender::SetRasterizerState(RASTERIZ_LAVEL::RS_CULL_BACK);

	LightUpdate(context, elapsedTime);
	Camera& camera = Camera::GetInstance();

	DirectX::XMMATRIX C, V, P, VP;
	C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
	V = DirectX::XMLoadFloat4x4(&camera.GetView());
	P = DirectX::XMLoadFloat4x4(&camera.GetProjection());
	VP = C * V * P;
	FLOAT4X4 vp;
	DirectX::XMStoreFloat4x4(&vp, VP);
	
	renderer->Begin(context, vp);
	{
		renderer->Draw(context, player->GetModel());
	}
	renderer->End(context);

	obj1->Render(context, camera.GetView(), camera.GetProjection());
	//HitBoxRender::GetInstance().Render(context, camera.GetView(), camera.GetProjection());
}

void SceneGame::LightInit(ID3D11Device* device)
{
	Light::Init(device);
	lightBuffer = std::make_unique<ConstantBuffer<CbLight>>(device);
}

void SceneGame::LightUpdate(ID3D11DeviceContext* context, float elapsedTime)
{
	Camera& camera = Camera::GetInstance();

	float XM_PI = 3.141592654f;
	static float lightAngle = XM_PI;
	Light::SetAmbient(VECTOR3(0.5f, 0.5f, 0.5f));
	//ライト方向
	LightDir.x = sinf(lightAngle);
	//でっけえライト
	LightDir.y = 0.0f;
	LightDir.z = cosf(lightAngle);
	static float angle = XM_PI / 4;
	angle += elapsedTime;
	Light::SetDirLight(LightDir, VECTOR3(1.0f, 1.0f, 1.0f));

	lightBuffer->data.ambientColor = Light::Ambient;
	lightBuffer->data.lightDir = VECTOR4(0, -1, 0, 1);
	lightBuffer->data.lightColor = Light::DirLightColor;
	lightBuffer->data.eyePos.x = camera.GetEye().x;
	lightBuffer->data.eyePos.y = camera.GetEye().y;
	lightBuffer->data.eyePos.z = camera.GetEye().z;
	lightBuffer->data.eyePos.w = 1.0f;

	lightBuffer->Activate(context, 3);
}