#pragma once

#include <d3d11.h>
#include <wrl.h>
using Microsoft::WRL::ComPtr;
#include "vector.h"
#include <memory>

#include "constant_buffer.h"
#include "texture.h"
#include "shader.h"

class CubeMesh
{
private:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
		VECTOR2 texcoord;
		VECTOR4 color;
	};

	struct Mesh
	{
		ComPtr<ID3D11Buffer> vertexBuffer;
		int numV;
		ComPtr<ID3D11Buffer> indexBuffer;
		int numI;
	};
	Mesh mesh;

	std::shared_ptr<Texture> texture;
	bool tex = false;

	struct CB
	{
		FLOAT4X4 world;
		FLOAT4X4 WVP;
		VECTOR4 materialColor;
	};
	std::unique_ptr<ConstantBuffer<CB>> cb;

	std::unique_ptr<Shader> shader;

	VECTOR3 position = { 0.f, 0.f, 0.f };
	VECTOR3 scale = { 1.f, 1.f, 1.f };
	VECTOR3 angle = { 0.f, 0.f, 0.f };
	FLOAT4X4 world;

public:
	CubeMesh(ID3D11Device* device, const wchar_t* filename);
	~CubeMesh() {}

	void SetTexture(ID3D11Device* device, std::shared_ptr<Texture>& texture);
	void SetTexture(ID3D11Device* device, const wchar_t* filename);
	void Update();
	void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection);
	void SetPosition(const VECTOR3& position) { this->position = position; }
	void SetScale(const VECTOR3& scale) { this->scale = scale; }
	void SetAngle(const VECTOR3& angle) { this->angle = angle; }
};

