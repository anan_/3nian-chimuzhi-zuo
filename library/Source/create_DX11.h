#pragma once

#include "vector.h"

#include <D3D11.h>
#include <wrl.h>

#include "blend_state.h"
#include "depth_stencil_state.h"
#include "rasterizer_state.h"

using Microsoft::WRL::ComPtr;

class Create_DX11
{
private:
    // DirectX 11
    HRESULT DeviceInit(HWND hwnd, int screenWidth, int screenHeight, bool fullScreen);
    void DeviceRelease();

    HRESULT CreateDevice();
    HRESULT CreateSwapChain(const int screenWidth, const int screenHeight, const bool fullScreen);
    HRESULT CreateRenderTargetView();
    HRESULT CreateDepthStencilView(const int screenWidth, const int screenHeight);


    HWND WindowsInit(const char* windowName, const int screenWidth, const int screenHeight, bool fullScreen, HINSTANCE hInstance);
    void WindowsRelease();

public:

    bool Init(const char* windowName, const int screenWidth, const int screenHeight, bool fullScreen, HINSTANCE hInstance);
    void Release();
    bool GameLoop();
    void ClearScreen();
    void ScreenPresent(unsigned int sync_interval, unsigned int flags);
    void SetScreenColor(const float r, const float g, const float b);

    float GetElapsedTime();
    void ResetHighResolutionTimer();

    void RenderingBegin();
    void RenderingEnd();
    float GetAverageRenderingTime();
};

namespace GetCoreSystem
{
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetContext();
	HWND GetHWND();
	HINSTANCE GetHINSTANCE();
	BlendState* GetBlendState();
	DepthStencilState *GetDepthStencilState();
	RasterizerState* GetRasterizerState();
    ID3D11DepthStencilView* GetDepthStencilView();
    ID3D11RenderTargetView* GetRenderTargetView();
}

namespace SetRender
{
	void SetViewPort(int width, int height);
	void SetBlender(const int blendMode);
	void SetDepthStencilState(const int flg);
	void SetRasterizerState(const int rasterizMode);
}
