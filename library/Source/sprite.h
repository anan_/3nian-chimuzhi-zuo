#pragma once

#include <d3d11.h>
#include <wrl.h>
using Microsoft::WRL::ComPtr;
#include <memory>
#include "vector.h"

#include "texture.h"
#include "shader.h"

class Sprite
{
private:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
		VECTOR2 texcoord;
		VECTOR4 color;
	};
	ComPtr<ID3D11Buffer> vertexBuffer;
	std::unique_ptr<Texture> texture;

	ComPtr<ID3D11DepthStencilState> depthStencilState;

	std::unique_ptr<Shader> shader;

	struct CutTexture
	{
		VECTOR2 texPos;
		VECTOR2 texSize;
	};
	CutTexture cut;

public:
	Sprite(ID3D11Device* device, const wchar_t* filename);
	~Sprite() {}

	void Cut(const VECTOR2& texPos, const VECTOR2& texSize)
	{
		cut.texPos = texPos;
		cut.texSize = texSize;
	}

	void Render(ID3D11DeviceContext* context,
		float dx, float dy, float dw, float dh,
		float sx, float sy, float sw, float sh,
		float alpha = 1.0f);
	void Render(ID3D11DeviceContext* context,
		const VECTOR2& position, const VECTOR2 size,
		const VECTOR2& texPos, const VECTOR2& texSize,
		float alpha = 1.f);

	void Render(ID3D11DeviceContext* context,
		const VECTOR2& position, const VECTOR2 size,
		float alpha = 1.f)
	{
		Render(context,
			position, size,
			cut.texPos, cut.texSize,
			alpha);
	}
};