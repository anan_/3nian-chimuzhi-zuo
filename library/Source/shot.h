#pragma once

#include "shot_obj.h"
class Shot {
public:
    Shot(std::shared_ptr<GeometricPrimitive>geo);
    void Update();
    void Set(VECTOR3 pos, VECTOR3 angle);
private:
    std::shared_ptr<ShotObj> shot;
};