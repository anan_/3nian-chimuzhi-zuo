#include "shot_manager.h"
ShotManager* ShotManager::shotmanager = nullptr;
ShotManager::ShotManager(ID3D11Device* device)
{
    geo = std::make_shared<GeometricSphere>(device);
}

void ShotManager::Update()
{
    for (auto& s : shotobj) {
        shot->Update();
    }
}

void ShotManager::Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection)
{
    for (auto& s : shotobj) {
        geo->Render(context, view, projection);
    }
}

void ShotManager::VectorSet(VECTOR3 pos, VECTOR3 angle)
{
    for (auto& s : shotobj) {
        shot->Set(pos, angle);
    }
}

void ShotManager::ShotSet()
{
    shot = std::make_shared<Shot>(geo);
}

ShotManager::~ShotManager()
{
    shotobj.clear();
}

