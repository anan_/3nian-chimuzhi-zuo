#pragma once

#include "vector.h"
#include <vector>
#include <memory>
#include <assert.h>

#include "view.h"
#include "projection.h"

class Camera
{
private:
    static Camera* instance;

public:
    Camera();
    ~Camera() = default;

    void Update(float elapsedTime);

    void SetEye(const VECTOR3& eye) { this->eye = eye; }
    void SetFocus(const VECTOR3& focus) { this->focus = focus; }
    void SetDistance(float distance) { this->distance = distance; }
    void SetYawPitchRoll(float yaw, float pitch, float roll)
    {
        this->yaw = yaw;
        this->pitch = pitch;
        this->roll = roll;
    }
    //void SetIsMove(bool isMove) { this->isMove = isMove; }

    const VECTOR3& GetEye() const { return eye; }
    const VECTOR3& GetFocus() const { return focus; }
    const VECTOR3& GetFront() const { return front; }
    const VECTOR3& GetRight() const { return right; }
    const VECTOR3& GetUp() const { return up; }
    const float& GetDistance() const { return distance; }
    const FLOAT4X4& GetView() const
    {
        if (view) return view->GetView();
        else
        {
            assert(!"view = nullptr !!");
            return error;
        }
    }
    const FLOAT4X4& GetProjection() const 
    { 
        if (projection) return projection->GetProjection();
        else
        {
            assert(!"projection = nullptr !!");
            return error;
        }
    }

private:
    VECTOR3 eye = { 0.f, 0.f, -10.f };
    VECTOR3 focus = { 0.f, 0.f, 0.f };
    float distance = 10.f;

    float yaw = 0.f;
    float pitch = 0.f;
    float roll = 0.f;

    VECTOR3 up = { 0.f, 1.f, 0.f };
    VECTOR3 right = { 1.f, 0.f, 0.f };
    VECTOR3 front = { 0.f, 0.f, 1.f };

    bool isMove = false;

    std::unique_ptr<View> view = nullptr;
    std::unique_ptr<Projection> projection = nullptr;

    FLOAT4X4 error = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 }; // �G���[�p
    FLOAT4X4 world = { };

    void CalculateView();
    void CalculateView1();
    void CalculateView2();
    void SetAxisOfRotation(const FLOAT4X4& rotation);

public:
    static void Create()
    {
        if (instance != nullptr) return;
        instance = new Camera;
    }
    static Camera& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};
