
#include "shader.h"
#include "create_DX11.h"
#include "resource_manager.h"
using namespace GetCoreSystem;

bool Shader::CreateSprite(ID3D11Device* device)
{
	//D3D11_INPUT_ELEMENT_DESC layout[] = 
	//{
	//    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	//    { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	//};
	//UINT numElements = ARRAYSIZE(layout);

	//const char* vsName = "Data/shaders/cso/sprite_vs.cso";
	//const char* psName = "Data/shaders/cso/sprite_ps.cso";

	//ResourceManager::LoadVertexShaders(GetDevice(), vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

	//ResourceManager::LoadPixelShaders(GetDevice(), psName, pixel_shader.GetAddressOf());

	return true;
}

bool Shader::Create2D(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	// 入力レイアウト
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);
	const char* vsName = "Data/shaders/cso/2D_vs.cso";
	const char* psName = "Data/shaders/cso/2D_ps.cso";

	ResourceManager::LoadVertexShaders(GetDevice(), vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

	ResourceManager::LoadPixelShaders(GetDevice(), psName, pixel_shader.GetAddressOf());

	return true;
}

bool Shader::CreateSpriteBatch(ID3D11Device * device)
{
	D3D11_INPUT_ELEMENT_DESC	layout[] =
	{
		{ "POSITION",           0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,   0 },
		{ "TEXCOORD",           0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,   0 },
		{ "NDC_TRANSFORM",      0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD_TRANSFORM", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "COLOR",              0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};
	const char* vsName = "Data/shaders/cso/sprite_batch_vs.cso";
	const char* psName = "Data/shaders/cso/sprite_ps.cso";

	if(!ResourceManager::LoadVertexShaders(device, vsName,
		layout, ARRAYSIZE(layout),
		vertex_shader.GetAddressOf(), input_layout.GetAddressOf()))
	{ }
	if (!ResourceManager::LoadPixelShaders(device, psName, pixel_shader.GetAddressOf())) {}

	return true;
}

bool Shader::CreateBoard(ID3D11Device* device)
{
	D3D11_INPUT_ELEMENT_DESC layout[] = {
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	const char* vsName = "Data/shaders/cso/board_vs.cso";
	const char* psName = "Data/shaders/cso/board_ps.cso";

	ResourceManager::LoadVertexShaders(device, vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

	ResourceManager::LoadPixelShaders(device, psName, pixel_shader.GetAddressOf());

	return true;
}

bool Shader::CreateGeometric(ID3D11Device* device)
{
    D3D11_INPUT_ELEMENT_DESC layout[] = {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
    };
    UINT numElements = ARRAYSIZE(layout);

    const char* vsName = "Data/shaders/cso/geometric_primitive_vs.cso";
    const char* psName = "Data/shaders/cso/geometric_primitive_ps.cso";

    ResourceManager::LoadVertexShaders(device, vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

    ResourceManager::LoadPixelShaders(device, psName, pixel_shader.GetAddressOf());

    return true;
}

bool Shader::CreateModel(ID3D11Device* device)
{
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "WEIGHTS",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "BONES",    0, DXGI_FORMAT_R32G32B32A32_UINT,  0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	const char* vsName = "Data/shaders/cso/model_vs.cso";
	const char* psName = "Data/shaders/cso/model_ps.cso";

	ResourceManager::LoadVertexShaders(device, vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

	ResourceManager::LoadPixelShaders(device, psName, pixel_shader.GetAddressOf());

	return true;
}

bool Shader::CreateMesh(ID3D11Device* device)
{
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	const char* vsName = "Data/shaders/cso/meshs_vs.cso";
	const char* psName = "Data/shaders/cso/meshs_ps.cso";

	ResourceManager::LoadVertexShaders(device, vsName, layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf());

	ResourceManager::LoadPixelShaders(device, psName, pixel_shader.GetAddressOf());

	return true;
}


void Shader::Activate(ID3D11DeviceContext* context)
{
    context->IASetInputLayout(input_layout.Get());

    context->VSSetShader(vertex_shader.Get(), NULL, 0);
    context->PSSetShader(pixel_shader.Get(), NULL, 0);
    context->GSSetShader(geometric_shader.Get(), NULL, 0);
    context->HSSetShader(hull_shader.Get(), NULL, 0);
    context->DSSetShader(domain_shader.Get(), NULL, 0);
}

void Shader::DeActivate(ID3D11DeviceContext* context)
{
}
