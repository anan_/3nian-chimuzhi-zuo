
#include "hitbox_render.h"
#include "create_DX11.h"

HitBoxRender* HitBoxRender::instance = nullptr;

void HitBoxRender::SetSphere(ID3D11Device* device, const Collision::Sphere& s)
{
	GeometricObj obj;
	obj.SetGeometric(sphere);
	obj.SetPosition(s.position);
	obj.SetScale(VECTOR3(s.scale, s.scale, s.scale));
	sphereList.push_back(obj);

	list.push_back(obj);
}

void HitBoxRender::Update()
{
	for (auto&& s : sphereList)
	{
		s.Update();
	}
}
