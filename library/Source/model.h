#pragma once

#include <memory>
#include <d3d11.h>
#include "vector.h"
#include "model_resource.h"

class Model
{
public:
	Model(std::shared_ptr<ModelResource>& resource);
	~Model() {}

	struct Node
	{
		const char* name;
		Node* parent;
		VECTOR3 scale;
		VECTOR4 rotate;
		VECTOR3 translate;
		FLOAT4X4 localTransform;
		FLOAT4X4 worldTransform;
	};

	// アニメーション
	bool IsPlayAnimation() const { return currentAnimation >= 0; }
	void PlayAnimation(int animationIndex, bool loop = false);
	void UpdateAnimation(float elapsedTime);

	// 行列計算
	void CalculateLocalTransform();
	void CalculateWorldTransform(const DirectX::XMMATRIX& worldTransform);

	// 例ピック
	int RayPick(const VECTOR3& startPosition, const VECTOR3& endPosition,
		VECTOR3* outPosition, VECTOR3* outNormal, float* outLength);

	const std::vector<Node>& GetNodes() const { return nodes; }
	const ModelResource* GetModelResource() const { return modelResource.get(); }

private:
	std::shared_ptr<ModelResource> modelResource;
	std::vector<Node> nodes;
	int currentAnimation = -1;
	float currentSeconds = 0.0f;
	bool loopAnimation = false;
	bool endAnimation = false;
};
