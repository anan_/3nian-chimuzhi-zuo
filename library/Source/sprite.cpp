
#include "sprite.h"
#include "window.h"
#include "create_DX11.h"

Sprite::Sprite(ID3D11Device* device,  const wchar_t* filename)
{
	Vertex v[] = {
	VECTOR3(-0.5f, 0.5f,0), VECTOR3(0,0,1), VECTOR2(0,0), VECTOR4(1,1,1,1), //左上
	VECTOR3(0.5f, 0.5f,0),  VECTOR3(0,0,1), VECTOR2(1,0), VECTOR4(1,1,1,1), //右上
	VECTOR3(-0.5f,-0.5f,0), VECTOR3(0,0,1), VECTOR2(0,1), VECTOR4(1,1,1,1), //左下
	VECTOR3(0.5f,-0.5f,0),  VECTOR3(0,0,1), VECTOR2(1,1), VECTOR4(1,1,1,1), //右下
	};

	//	頂点バッファ作成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd)); // 全０
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(v);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA res;
	ZeroMemory(&res, sizeof(res));
	res.pSysMem = v;

	device->CreateBuffer(&bd, &res, vertexBuffer.GetAddressOf());

	//	テクスチャ読み込み
	texture = std::make_unique<Texture>();
	texture->Load(device, filename);

	//デプスステンシルステート
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
	depth_stencil_desc.DepthEnable = FALSE;
	device->CreateDepthStencilState(&depth_stencil_desc, depthStencilState.GetAddressOf());

	shader = std::make_unique<Shader>();
	shader->Create2D(device);
}

void Sprite::Render(ID3D11DeviceContext* context, float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh, float alpha)
{
	SetRender::SetRasterizerState(RASTERIZ_LAVEL::RS_CULL_NONE);

	shader->Activate(context);
	//頂点データ設定
	Vertex data[4];

	data[0].position.x = dx;
	data[0].position.y = dy;
	data[0].position.z = 0.0f;

	data[1].position.x = dx + dw;
	data[1].position.y = dy;
	data[1].position.z = 0.0f;

	data[2].position.x = dx;
	data[2].position.y = dy + dh;
	data[2].position.z = 0.0f;

	data[3].position.x = dx + dw;
	data[3].position.y = dy + dh;
	data[3].position.z = 0.0f;

	// 正規化デバイス座標系
	for (int i = 0; i < 4; i++) {
		data[i].position.x = 2.0f * data[i].position.x / SCREEN_WIDTH - 1.0f;
		data[i].position.y = 1.0f - 2.0f * data[i].position.y / SCREEN_HEIGHT;
		data[i].position.z = 0.0f;
	}

	//テクスチャ座標設定
	data[0].texcoord.x = sx;
	data[0].texcoord.y = sy;
	data[1].texcoord.x = sx + sw;
	data[1].texcoord.y = sy;
	data[2].texcoord.x = sx;
	data[2].texcoord.y = sy + sh;
	data[3].texcoord.x = sx + sw;
	data[3].texcoord.y = sy + sh;

	//UV座標
	for (int i = 0; i < 4; i++) {
		data[i].texcoord.x = data[i].texcoord.x / texture->GetWidth();
		data[i].texcoord.y = data[i].texcoord.y / texture->GetHeight();
	}
	//頂点カラー
	data[0].color = VECTOR4(1, 1, 1, alpha);
	data[1].color = VECTOR4(1, 1, 1, alpha);
	data[2].color = VECTOR4(1, 1, 1, alpha);
	data[3].color = VECTOR4(1, 1, 1, alpha);
	//法線
	data[0].normal = VECTOR3(0, 0, 1);
	data[1].normal = VECTOR3(0, 0, 1);
	data[2].normal = VECTOR3(0, 0, 1);
	data[3].normal = VECTOR3(0, 0, 1);

	//頂点データ更新
	context->UpdateSubresource(vertexBuffer.Get(), 0, NULL, data, 0, 0);

	//	頂点バッファの指定
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(
		0, 1, vertexBuffer.GetAddressOf(), // スロット, 数, バッファ
		&stride,		// １頂点のサイズ
		&offset			// 開始位置
	);
	context->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);
	//テクスチャの設定
	texture->Set(context);

	context->Draw(4, 0);

	texture->Set(context, 0, false);
	SetRender::SetRasterizerState(RASTERIZ_LAVEL::RS_CULL_BACK);
}

void Sprite::Render(ID3D11DeviceContext* context, const VECTOR2& position, const VECTOR2 size, const VECTOR2& texPos, const VECTOR2& texSize, float alpha)
{
	Render(context,
		position.x, position.y, size.x, size.y,
		texPos.x, texPos.y, texSize.x, texSize.y,
		alpha);
}
