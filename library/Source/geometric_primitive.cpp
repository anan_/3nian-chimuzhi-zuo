
#include	"geometric_primitive.h"

#include	<vector>
#include	"resource_manager.h"
#include "create_DX11.h"

bool GeometricPrimitive::CreateBuffers(ID3D11Device* device, Vertex* vertices, int numV, unsigned int* indices, int numI)
{
    HRESULT hr = S_OK;
  
	mesh.numV = numV;
	D3D11_BUFFER_DESC desc;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numV;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	D3D11_SUBRESOURCE_DATA data;
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = vertices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.vertexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	mesh.numI = numI;
	{
		ZeroMemory(&desc, sizeof(desc));
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(Vertex) * mesh.numI;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	}
	{
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = indices;
	}
	hr = device->CreateBuffer(&desc, &data, mesh.indexBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(!"CreateBuffer");
	}

	cbColor = std::make_unique<ConstantBuffer<CBColor>>(device);
	cbMatrix = std::make_unique<ConstantBuffer<CBMatrix>>(device);

    return true;
}

void GeometricPrimitive::SetCbMatrix(ID3D11DeviceContext* context, const FLOAT4X4& world, const FLOAT4X4& view, const FLOAT4X4& projection)
{
	cbMatrix->data.world = world;
	DirectX::XMMATRIX w, v, p, wvp;
	w = DirectX::XMLoadFloat4x4(&world);
	v = DirectX::XMLoadFloat4x4(&view);
	p = DirectX::XMLoadFloat4x4(&projection);
	wvp = w * v * p;
	DirectX::XMStoreFloat4x4(&cbMatrix->data.WVP, wvp);

	cbMatrix->Activate(context, 1);
}

void GeometricPrimitive::Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color)
{
	shader->Activate(context);

	cbColor->data.materialColor = color;
	cbColor->Activate(context, 0);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	context->IASetVertexBuffers(0, 1, mesh.vertexBuffer.GetAddressOf(), &stride, &offset);
	context->IASetIndexBuffer(mesh.indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	//SetRender::SetRasterizerState(RASTERIZ_LAVEL::RS_CULL_NONE);
	context->DrawIndexed(mesh.numI, 0, 0);

	//SetRender::SetRasterizerState(RASTERIZ_LAVEL::RS_CULL_BACK);
}

GeometricCube::GeometricCube(ID3D11Device* device) : GeometricPrimitive(device)
{
	int numVertices, numIndices;

	Vertex vertices[24] =
	{
		VECTOR3(0.5f,	0.5f,  -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f),
		VECTOR3(-0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f),
		VECTOR3(0.5f,  -0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f),
		VECTOR3(-0.5f, -0.5f, -0.5f),	VECTOR3(0.0f, 0.0f, -1.0f),

		VECTOR3(-0.5f,	0.5f, 0.5f),	VECTOR3(0.0f, 0.0f, 1.0f),
		VECTOR3(0.5f,  0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),
		VECTOR3(-0.5f,-0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),
		VECTOR3(0.5f, -0.5f, 0.5f),		VECTOR3(0.0f, 0.0f, 1.0f),

		VECTOR3(0.5f,	0.5f, 0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),
		VECTOR3(0.5f,  0.5f, -0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),
		VECTOR3(0.5f, -0.5f, 0.5f),	    VECTOR3(1.0f, 0.0f, 0.0f),
		VECTOR3(0.5f, -0.5f, -0.5f),	VECTOR3(1.0f, 0.0f, 0.0f),

		VECTOR3(-0.5f,	0.5f, -0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f),
		VECTOR3(-0.5f,  0.5f, 0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f),
		VECTOR3(-0.5f, -0.5f, -0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f),
		VECTOR3(-0.5f, -0.5f, 0.5f),	VECTOR3(-1.0f, 0.0f, 0.0f),

		VECTOR3(0.5f,	 0.5f,  0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),
		VECTOR3(-0.5f,  0.5f,  0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),
		VECTOR3(0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),
		VECTOR3(-0.5f,  0.5f, -0.5f),	VECTOR3(0.0f, 1.0f, 0.0f),

		VECTOR3(0.5f,	 -0.5f, -0.5f),	VECTOR3(0.0f, -1.0f, 0.0f),
		VECTOR3(-0.5f,  -0.5f, -0.5f),	VECTOR3(0.0f, -1.0f, 0.0f),
		VECTOR3(0.5f,  -0.5f, 0.5f),	VECTOR3(0.0f, -1.0f, 0.0f),
		VECTOR3(-0.5f,  -0.5f, 0.5f),	VECTOR3(0.0f, -1.0f, 0.0f),

	};
	numVertices = sizeof(vertices) / sizeof(vertices[0]);

	unsigned int indices[36];
	for (int face = 0; face < 6; face++)
	{
		indices[face * 6] = face * 4;
		indices[face * 6 + 1] = face * 4 + 2;
		indices[face * 6 + 2] = face * 4 + 1;
		indices[face * 6 + 3] = face * 4 + 1;
		indices[face * 6 + 4] = face * 4 + 2;
		indices[face * 6 + 5] = face * 4 + 3;
	}
	numIndices = sizeof(indices) / sizeof(indices[0]);

	CreateBuffers(device, vertices, numVertices, indices, numIndices);
}

//GeometricRect::GeometricRect(ID3D11Device* device) : GeometricPrimitive(device)
//{
//	//vertices.resize(4);
//	//indices.resize(3 * 2);
//
//	//CreateBuffers(device, vertices, indices);
//}
//
//GeometricBoard::GeometricBoard(ID3D11Device* device) : GeometricPrimitive(device)
//{
//	int numVertices, numIndices;
//	Vertex vertices[4] = {};
//	unsigned int indices[3 * 2] = {};
//
//	vertices[0].position = VECTOR3(-0.5f, +0.5f, .0f);
//	vertices[1].position = VECTOR3(+0.5f, +0.5f, .0f);
//	vertices[2].position = VECTOR3(-0.5f, -0.5f, .0f);
//	vertices[3].position = VECTOR3(+0.5f, -0.5f, .0f);
//	vertices[0].normal = vertices[1].normal =
//		vertices[2].normal =
//		vertices[3].normal = VECTOR3(+0.0f, +0.0f, -1.0f);
//	indices[0] = 0;	indices[1] = 1;	indices[2] = 2;
//	indices[3] = 1;	indices[4] = 3;	indices[5] = 2;
//
//	numVertices = sizeof(vertices);
//	numIndices = sizeof(indices);
//
//	CreateBuffers(device, vertices, numVertices, indices, numIndices);
//}
//
//GeometricBoard::GeometricBoard(ID3D11Device* device, int row, int col)
//{
//}

GeometricSphere::GeometricSphere(ID3D11Device* device, int slices, int stacks) : GeometricPrimitive(device)
{
	std::vector<Vertex> vertices;
	std::vector<u_int> indices;


	float r = 0.5f;		//	���a 0.5f = ���a 1.0f

	//
	// Compute the vertices stating at the top pole and moving down the stacks.
	//

	// Poles: note that there will be texture coordinate distortion as there is
	// not a unique point on the texture map to assign to the pole when mapping
	// a rectangular texture onto a sphere.
	Vertex top_vertex;
	top_vertex.position = VECTOR3(0.0f, +r, 0.0f);
	top_vertex.normal = VECTOR3(0.0f, +1.0f, 0.0f);

	Vertex bottom_vertex;
	bottom_vertex.position = VECTOR3(0.0f, -r, 0.0f);
	bottom_vertex.normal = VECTOR3(0.0f, -1.0f, 0.0f);

	vertices.push_back(top_vertex);

	float phi_step = DirectX::XM_PI / stacks;
	float theta_step = 2.0f * DirectX::XM_PI / slices;

	// Compute vertices for each stack ring (do not count the poles as rings).
	for (u_int i = 1; i <= static_cast<u_int>(stacks - 1); ++i)
	{
		float phi = i * phi_step;
		float rs_phi = r * sinf(phi), rc_phi = r * cosf(phi);

		// Vertices of ring.
		for (u_int j = 0; j <= static_cast<u_int>(slices); ++j)
		{
			float theta = j * theta_step;

			Vertex v;

			// spherical to cartesian
			v.position.x = rs_phi * cosf(theta);
			v.position.y = rc_phi;
			v.position.z = rs_phi * sinf(theta);

			DirectX::XMVECTOR p = DirectX::XMLoadFloat3(&v.position);
			DirectX::XMStoreFloat3(&v.normal, DirectX::XMVector3Normalize(p));

			vertices.push_back(v);
		}
	}

	vertices.push_back(bottom_vertex);

	//
	// Compute indices for top stack.  The top stack was written first to the vertex buffer
	// and connects the top pole to the first ring.
	//
	for (UINT i = 1; i <= static_cast<u_int>(slices); ++i)
	{
		indices.push_back(0);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	//
	// Compute indices for inner stacks (not connected to poles).
	//

	// Offset the indices to the index of the first vertex in the first ring.
	// This is just skipping the top pole vertex.
	u_int base_index = 1;
	u_int ring_vertex_count = slices + 1;
	for (u_int i = 0; i < static_cast<u_int>(stacks - 2); ++i)
	{
		u_int i_rvc = i * ring_vertex_count;
		u_int i1_rvc = (i + 1) * ring_vertex_count;

		for (u_int j = 0; j < static_cast<u_int>(slices); ++j)
		{
			indices.push_back(base_index + i_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j);

			indices.push_back(base_index + i1_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j + 1);
		}
	}

	//
	// Compute indices for bottom stack.  The bottom stack was written last to the vertex buffer
	// and connects the bottom pole to the bottom ring.
	//

	// South pole vertex was added last.
	u_int south_pole_index = (u_int)vertices.size() - 1;

	// Offset the indices to the index of the first vertex in the last ring.
	base_index = south_pole_index - ring_vertex_count;

	for (u_int i = 0; i < static_cast<u_int>(slices); ++i)
	{
		indices.push_back(south_pole_index);
		indices.push_back(base_index + i);
		indices.push_back(base_index + i + 1);
	}

	CreateBuffers(device, vertices.data(), static_cast<int>(vertices.size()), indices.data(), static_cast<int>(indices.size()));
}

//GeometricSphere2::GeometricSphere2(ID3D11Device* device, u_int div)
//{
//	//vertices.resize(4);
//	//indices.resize(3 * 2);
//
//	//CreateBuffers(device, vertices, indices);
//}
