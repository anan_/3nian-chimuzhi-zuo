#pragma once

#include "vector.h"

class View
{
public:
	View() {}
	~View() {}

	void CalculateView(const VECTOR3& eye, const VECTOR3& focus, const VECTOR3& upVector);
	void CalculateView(const FLOAT4X4& world);

	const FLOAT4X4& GetView()
	{
		return view;
	}

private:
	FLOAT4X4 view;
};