#pragma once
#include "geometric_primitive.h"
#include "shot.h"
#include <vector>
class ShotManager {
public:
    static void Create(ID3D11Device* device) {
        if (shotmanager != nullptr)return;
        shotmanager = new ShotManager(device);
    }
    static void Destroy() {
        if (shotmanager == nullptr)return;
        delete shotmanager;
        shotmanager = nullptr;
    }
    void Update();
    void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection);
    void VectorSet(VECTOR3 pos, VECTOR3 angle);
    static ShotManager* GetInstsnce() {
        return shotmanager;
    }
    void ShotSet();
    void ObjSet(std::shared_ptr<ShotObj>obj) { shotobj.push_back(obj); }
    std::vector<std::shared_ptr<ShotObj>>GetShot() { return shotobj; }
    ~ShotManager();
private:
    ShotManager(ID3D11Device* device);
    std::vector<std::shared_ptr<ShotObj>>shotobj;
    std::shared_ptr<Shot>shot;
    std::shared_ptr<GeometricSphere>geo;
    static ShotManager* shotmanager;
};
#define pShotManager (ShotManager::GetInstsnce())