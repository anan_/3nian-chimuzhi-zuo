#include "player_move.h"

PlayerMove::PlayerMove()
{
	SetAcceleration(10.0f);
	SetDeceleration(5.0f);
	SetMaxSpeed(50.0f);
}

void PlayerMove::Move(Obj3D* obj, float elapsedTime)
{
	Camera& camera = Camera::GetInstance();

	const VECTOR2& PAD = EmulateGamePad::WASD();
	const VECTOR3& right = camera.GetRight();
	const VECTOR3& up = camera.GetUp();
	const VECTOR3& front = camera.GetFront();

	VECTOR3 force;
	force.x = (right.x * PAD.x) + (front.x * PAD.y);
	force.y = (-right.z * PAD.x) + (-front.z * PAD.y);
	//force.z = (-right.z * PAD.x) + (-front.z * PAD.y);

	AddForce(force);
	VECTOR3 rotation = obj->GetRotation();
	if (PAD.x >= 1.0f)
	{
		rotation.y += 0.025f;
	}
	if (PAD.x <= -1.0f)
	{
		rotation.y -= 0.025f;
	}

	if (PAD.y >= 1.0)
	{
		rotation.x += 0.025f;
	}
	if (PAD.y <= -1.0)
	{
		rotation.x -= 0.025f;
	}

	obj->SetRotation(rotation);
	playerMove(obj, elapsedTime);
}
