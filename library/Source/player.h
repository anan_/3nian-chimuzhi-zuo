#pragma once

#include <d3d11.h>
#include "obj3d.h"
#include <memory>
#include "model_resource.h"
#include "camera.h"
#include "player_move.h"

class Player
{
public:
	Player(ID3D11Device* device, std::shared_ptr<ModelResource>& resource);
	~Player(){}
	void Update(float elapsedTime);

	Model* GetModel() { return playerObj->GetModel(); }

private:
	std::unique_ptr<Obj3D> playerObj = nullptr;
	std::shared_ptr<PlayerMove> playerMove = nullptr;
};