#pragma once

#include "create_DX11.h"
#include "geometric_obj.h"
#include "collision.h"
#include <vector>

class HitBoxRender
{
	static HitBoxRender* instance;

public:
	HitBoxRender(ID3D11Device* device)
	{
		cube = std::make_shared<GeometricCube>(device);
		sphere = std::make_shared<GeometricSphere>(device);
	}
	~HitBoxRender() {}

	void SetSphere(ID3D11Device* device, const Collision::Sphere& s);
	void Update();
	void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection)
	{
		for (auto& s : sphereList)
		{
			s.Render(context, view, projection);
		}

		sphereList.clear();
	}

	//void SetScale(const VECTOR3& scale) { obj->SetScale(scale); };
	//void SetAngle(const VECTOR3& angle) { obj->SetAngle(angle); }
	//void SetPosition(const VECTOR3& position) { obj->SetPosition(position); }

	//const VECTOR3& GetScale() { return obj->GetScale(); }
	//const VECTOR3& GetAngle() { return obj->GetAngle(); }
	//const VECTOR3& GetPosition() { return obj->GetPosition(); }

protected:
	//std::unique_ptr<GeometricObj> obj;

	std::shared_ptr<GeometricPrimitive> cube;
	std::shared_ptr<GeometricPrimitive> sphere;
	std::vector<GeometricObj> sphereList;
	std::vector<GeometricObj> list;

public:
	static void Create(ID3D11Device* device)
	{
		if (instance != nullptr) return;
		instance = new HitBoxRender(device);
	}
	static HitBoxRender& GetInstance()
	{
		return *instance;
	}
	static void Destory()
	{
		if (instance != nullptr)
		{
			delete instance;
			instance = nullptr;
		}
	}
};