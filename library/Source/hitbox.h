#pragma once

#include "geometric_primitive.h"

class HitBox
{
public:
	HitBox() {}
	~HitBox() {}

	void SetGeometric(std::shared_ptr<GeometricPrimitive>& obj)
	{
		this->obj = obj;
	}

	void Update();
	void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection);

	void SetScale(const VECTOR3& scale) { obj->SetScale(scale); };
	void SetAngle(const VECTOR3& angle) { obj->SetAngle(angle); }
	void SetPosition(const VECTOR3& position) { obj->SetPosition(position); }

private:
	std::shared_ptr<GeometricPrimitive> obj = nullptr;
};