#pragma once

#include "camera.h"

class CameraControl
{
public:
	CameraControl();
	~CameraControl() {}
	
	void Activate(float elapsedtime);

private:
	VECTOR3 position = {0.f, 0.f, 0.f};
	VECTOR3 target = { 0.f, 0.f, 0.f };
	VECTOR3 move = { 0.f, 0.f, 0.f };
	const float speed = 0.05f;
	float yaw = 0.f;
	float pitch = 0.f;
	float roll = 0.f;
	float angle = 0.f;

	float offsetZ = 0.f;

	
	void DebugCamera01(float elapsedtime);
	void DebugCamera02(float elapsedtime);

	void CameraMove(float elapsedtime);

	void FPSCamera(float elapsedtime);
	void TPSCamera(float elapsedTime);
};