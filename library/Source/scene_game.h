#pragma once

#include "scene.h"
#include "constant_buffer.h"
#include "model_renderer.h"
#include "camera_control.h"
#include "light.h"

#include "sprite.h"

#include "cube_mesh.h"
#include "geometric_primitive.h"
#include "geometric_obj.h"

#include "collision.h"

#include "player.h"

class SceneGame : public Scene
{
private:
	std::unique_ptr<ModelRenderer> renderer;

	struct CbLight
	{
		VECTOR4 lightColor;
		VECTOR4 lightDir;
		VECTOR4 ambientColor;
		VECTOR4 eyePos;
		POINTLIGHT PointLight[Light::POINTMAX];
		SPOTLIGHT SpotLight[Light::SPOTMAX];
	};
	std::unique_ptr<ConstantBuffer<CbLight>> lightBuffer;
	VECTOR3 LightDir;

	std::unique_ptr<CameraControl> cameraControl;

	std::unique_ptr<Sprite> sp;

	std::unique_ptr<CubeMesh> cu;
	std::unique_ptr<GeometricObj> obj1;
	std::unique_ptr<GeometricObj> obj2;

	std::unique_ptr<Player> player;

	Collision::Sphere s1;
	Collision::Sphere s2;

	void LightInit(ID3D11Device* device);
	void LightUpdate(ID3D11DeviceContext* context, float elapsedTime);

public:
	void Init(ID3D11Device* device);
	void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);
};