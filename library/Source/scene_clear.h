#pragma once

#include "scene.h"

class SceneClear : public Scene
{
public:
    void Init(ID3D11Device* device);
    void Release();
	void Update(ID3D11Device* device, float elapsedTime);
	void Render(ID3D11DeviceContext* context, float elapsedTime);
};
