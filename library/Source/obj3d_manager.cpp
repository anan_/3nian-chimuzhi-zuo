
#include "obj3d_manager.h"
#include "load_model.h"

Obj3DManager::Obj3DManager(ID3D11Device* device)
{
	objList.clear();
	renderer = std::make_unique<ModelRenderer>(device);
}

Obj3D* Obj3DManager::Add(
    ID3D11Device* device,
    const char* modelTag,
    std::shared_ptr<MoveAlg> mvAlg,
    const VECTOR3& scale, const VECTOR3& rotation, const VECTOR3& position,
    bool exist)
{
    Obj3D obj;
    obj.SetMoveAlg(mvAlg);
    obj.SetExist(exist);
    obj.SetScale(scale);
    obj.SetRotation(rotation);
    obj.SetPosition(position);
    obj.CalculateWorld();
    obj.SetModelResource(pLoadModel.GetModelResource(modelTag));

    objList.push_back(obj);
    // 逆向きのイテレーター取得 (末尾が返ってくる)
    return &(*objList.rbegin());
}

void Obj3DManager::Update(float elapsedTime)
{
    for (auto&& it : objList)
    {
        if (!it.GetExist()) continue;
        it.Update(elapsedTime);
    }

    auto&& it = objList.begin();
    while (it != objList.end())
    {
        if (it->GetMoveAlg())
        {
            it++;
        }
        else
        {
            it = objList.erase(it);
        }
    }
}

void Obj3DManager::Render(ID3D11DeviceContext* context, const FLOAT4X4& viewProjection)
{
    renderer->Begin(context, viewProjection);
    for (auto&& it : objList)
    {
        if (!it.GetExist()) continue;
        renderer->Draw(context, it.GetModel());
    }
    renderer->End(context);
}


