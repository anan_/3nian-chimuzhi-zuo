#pragma once
#include "obj3d.h"
#include "geometric_primitive.h"

class ShotObj :public Obj3D {
public:
    ShotObj(std::shared_ptr<GeometricPrimitive> geo);
    void Update();
    GeometricPrimitive* GetSphere() { return Sphere.get(); }
private:
    std::shared_ptr<GeometricPrimitive>Sphere;
};