
#include "player.h"
#include "emulate_gamepad.h"
Player::Player(ID3D11Device* device, std::shared_ptr<ModelResource>& resource)
{

	playerObj = std::make_unique<Obj3D>();
	playerObj->SetScale(VECTOR3(0.01f, 0.01f, 0.01f));
	playerObj->SetPosition(VECTOR3(0.f, -5.f, 0.f));
	playerObj->SetRotation(VECTOR3(0.f, 0.f, 0.f));
	playerObj->SetModelResource(resource);
	playerMove = std::make_shared<PlayerMove>();
	playerObj->SetMoveAlg(playerMove);

	//playerObj->Update(0.f);
}

void Player::Update(float elapsedTime)
{
	playerObj->Update(elapsedTime);
}
