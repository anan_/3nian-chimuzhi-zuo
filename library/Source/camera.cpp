
#include "camera.h"
#include "create_DX11.h"
#include "window.h"
#include "input.h"
#include "mouse.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Camera* Camera::instance = nullptr;

Camera::Camera()
{
	instance = this;

	view = std::make_unique<View>();
	projection = std::make_unique<Projection>();

	view->CalculateView(eye, focus, up);
	projection->CalculatePerspective(30.f, SCREEN_WIDTH / SCREEN_HEIGHT);
}

void Camera::Update(float elapsedTime)
{
	//CalculateView();
	view->CalculateView(eye, focus, up);
	front = CreateVec3(eye, focus);
	Vec3Normalize(&front, front);

#ifdef USE_IMGUI
	ImGui::Begin("Camera");

	ImGui::Text("Eye x:%.3f y:%.3f z:%.3f", eye.x, eye.y, eye.z);
	ImGui::Text("Focus x:%.3f y:%.3f z:%.3f", focus.x, focus.y, focus.z);
	ImGui::Text("yaw:%.3f pitch:%.3f roll:%.3f", yaw, pitch, roll);
	
	ImGui::Text("Front x:%.3f y:%.3f z:%.3f", front.x, front.y, front.z);
	ImGui::Text("Right x:%.3f y:%.3f z:%.3f", right.x, right.y, right.z);
	ImGui::Text("Up x:%.3f y:%.3f z:%.3f", up.x, up.y, up.z);

	ImGui::End();
#endif
}

void Camera::CalculateView()
{
	//DirectX::XMMATRIX R, T;
	//R = DirectX::XMMatrixRotationRollPitchYaw(pitch, yaw, roll);
	//T = DirectX::XMMatrixTranslation(eye.x, eye.y, eye.z);

	//DirectX::XMStoreFloat4x4(&world, R * T);
	//view->CalculateView(world);

	//FLOAT4X4 r;
	//DirectX::XMStoreFloat4x4(&r, R);
	//SetAxisOfRotation(r);
}

void Camera::CalculateView1()
{
	//DirectX::XMVECTOR eyeMat = DirectX::XMLoadFloat3(&eye);

	//// Z���ŉ�]
	//DirectX::XMMATRIX ZRot = DirectX::XMMatrixRotationZ(roll);
	//DirectX::XMMATRIX W = DirectX::XMLoadFloat4x4(&world);
	//ZRot = ZRot * W;

	//// �ܓx�o�x�̍����ړ�
	//DirectX::XMVECTOR DL;
	//DL = DirectX::XMVector3TransformCoord(DirectX::XMLoadFloat3(&moveVec), ZRot);

	//DirectX::XMVECTOR RotAxis, *ZAxis;
	//ZAxis = &ZRot.r[2];
	//RotAxis = DirectX::XMVector3Cross(DL, *ZAxis);

	//DirectX::XMVECTOR transQ;
	//transQ = DirectX::XMQuaternionRotationAxis(RotAxis, unit);
	//DirectX::XMMATRIX transRot;
	//transRot = DirectX::XMMatrixRotationQuaternion(transQ);
	//eyeMat = DirectX::XMVector3TransformCoord(eyeMat, transRot);

	//VECTOR3 X, Y, Z, eyeToFocus, _eye;
 //   DirectX::XMStoreFloat3(&_eye, eyeMat);
	//// Z
	//eyeToFocus = CreateVec3(_eye, focus);
	//Vec3Normalize(&Z, eyeToFocus);
	//// ��Y
	//Y = up;
	//// X
	//Vec3Cross(&X, Y, Z);
	//Vec3Normalize(&X, X);
	//// Y
	//Vec3Cross(&Y, Z, X);
	//Vec3Normalize(&Y, Y);

	//front = Z;
	//right = X;
	//up = Y;

	//DirectX::XMVECTOR _X, _Y, _Z;
	//_X = DirectX::XMLoadFloat3(&X);
	//_Y = DirectX::XMLoadFloat3(&Y);
	//_Z = DirectX::XMLoadFloat3(&Z);
	//DirectX::XMMATRIX _V;
	//_V.r[0] = _X;
	//_V.r[1] = _Y;
	//_V.r[2] = _Z;

	////EYE = DirectX::XMLoadFloat3(&-eye);
	////distance = Vec3Length(eye);
	////VECTOR3 EYE = -eye;
	////Vec3Normalize(&EYE, EYE);
	////eye +=  EYE;
	////distance -= offsetZ;

	//VECTOR3 posW = _eye + focus;
	//_V.r[3] = DirectX::XMVectorSet(posW.x, posW.y, posW.z, 1.0f);

	////

	//DirectX::XMStoreFloat4x4(&world, _V);
}

void Camera::CalculateView2()
{
	const float sx = ::sinf(pitch);
	const float cx = ::cosf(pitch);
	const float sy = ::sinf(yaw);
	const float cy = ::cosf(yaw);
	
	front = VECTOR3(-cx * sy, -sx, -cx * cy);
	right = VECTOR3(cy, 0, -sy);
	Vec3Cross(&up, right, front);

	eye = focus - (front * distance);
	Vec3Normalize(&front, front);
	Vec3Normalize(&right, right);
	Vec3Normalize(&up, up);
	view->CalculateView(eye, focus, up);
}

void Camera::SetAxisOfRotation(const FLOAT4X4& rotation)
{
	front = VECTOR3(rotation._31, rotation._32, rotation._33);
	front = VECTOR3(rotation._11, rotation._12, rotation._13);
	up = VECTOR3(rotation._21, rotation._22, rotation._23);
	Vec3Normalize(&front, front);
	Vec3Normalize(&right, right);
	Vec3Normalize(&up, up);
}


