
#include "hitbox.h"

void HitBox::Update()
{
	obj->Update();
}

void HitBox::Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection)
{
	obj->Render(context, view, projection);
}
