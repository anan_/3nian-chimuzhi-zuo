#include "shot_obj.h"

ShotObj::ShotObj(std::shared_ptr<GeometricPrimitive> geo)
{
    Sphere = geo;
}

void ShotObj::Update()
{
    CalculateWorld();
}
