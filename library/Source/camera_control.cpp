
#include "camera_control.h"
#include "create_DX11.h"
#include "window.h"
#include "input.h"
#include "mouse.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

CameraControl::CameraControl()
{
	//Camera& camera = Camera::GetInstance();
	//camera.pr.rotation.SetYawPitchRoll(0.f, PI * 0.5f, 0.f);
	//camera.pr.position = camera.GetEye();
}

void CameraControl::Activate(float elapsedtime)
{
	Camera& camera = Camera::GetInstance();
	position = camera.GetEye();

	CameraMove(elapsedtime);
	//DebugCamera01(elapsedtime);
	FPSCamera(elapsedtime);
}

void CameraControl::DebugCamera01(float elapsedtime)
{
}

void CameraControl::DebugCamera02(float elapsedtime)
{
	static Key ALT(VK_MENU);
	if (ALT.state(TRIGGER_MODE::NONE))
	{
		Camera& camera = Camera::GetInstance();
		Mouse& mouse = Mouse::GetInstance();

		if (mouse.LeftButtonState(TRIGGER_MODE::NONE))
		{
			const VECTOR2& move = mouse.GetMove();

			// Y��]
			yaw += move.x * 0.2f;
			if (yaw > PI)
			{
				yaw -= PI * 2.f;
			}
			else if (yaw < -PI)
			{
				yaw += PI * 2.f;
			}
			// X��]
			pitch += move.y * 0.2f;
			if (pitch > PI)
			{
				pitch += PI * 2.f;
			}
			else if (pitch < -PI)
			{
				pitch -= PI * 2.f;
			}
			camera.SetYawPitchRoll(yaw, pitch, 0.f);
		}
		// ���s�ړ�
		else if (mouse.CenterButtonState(TRIGGER_MODE::NONE))
		{
			const float s = camera.GetDistance() * 0.035f;
			const float x = -mouse.GetMove().x * s;
			const float y = mouse.GetMove().y * s;
			const VECTOR3& up = camera.GetUp();
			const VECTOR3& right = camera.GetRight();

			VECTOR3 focus = camera.GetFocus();
			focus += right * x;
			focus += up * y;

			camera.SetFocus(focus);
		}
		// �Y�[��
		else if (mouse.RightButtonState(TRIGGER_MODE::NONE))
		{
			const VECTOR2& move = mouse.GetMove();
			float dist = camera.GetDistance();
			dist += (-move.y - move.x) * dist * 0.1f;
			camera.SetDistance(dist);
		}
	}
}

void CameraControl::CameraMove(float elapsedtime)
{
	Camera& camera = Camera::GetInstance();
	static Key UP(VK_UP), DOWN(VK_DOWN), RIGHT(VK_RIGHT), LEFT(VK_LEFT);

	if (UP.state(TRIGGER_MODE::NONE)) position.z += speed;
	if (DOWN.state(TRIGGER_MODE::NONE)) position.z -= speed;
	if (RIGHT.state(TRIGGER_MODE::NONE)) position.x += speed;
	if (LEFT.state(TRIGGER_MODE::NONE)) position.x -= speed;

	camera.SetEye(position);
}

void CameraControl::FPSCamera(float elapsedtime)
{
	Camera& camera = Camera::GetInstance();
	Mouse& mouse = Mouse::GetInstance();

	// ��]��������
	static Key W('W'), S('S'), A('A'), D('D'), Q('Q'), E('E');
	// y��
	if (A.state(TRIGGER_MODE::NONE)) yaw += ToRadian(1.f);
	if (D.state(TRIGGER_MODE::NONE)) yaw -= ToRadian(1.f);
	// x��
	if (W.state(TRIGGER_MODE::NONE)) pitch += ToRadian(1.f);
	if (S.state(TRIGGER_MODE::NONE)) pitch -= ToRadian(1.f);
	// z��
	if (Q.state(TRIGGER_MODE::NONE)) roll += ToRadian(1.f);
	if (E.state(TRIGGER_MODE::NONE)) roll -= ToRadian(1.f);
	// ��]�̐��K���H
	NormalizeAngle(yaw); NormalizeAngle(pitch); NormalizeAngle(roll);

	const VECTOR3& eye = camera.GetEye();
	const VECTOR3& front = camera.GetFront();
	const float sx = ::sinf(pitch);
	const float cx = ::cosf(pitch);
	const float sy = ::sinf(yaw);
	const float cy = ::cosf(yaw);
	VECTOR3 rot = { -cx * sy, sx, cx * cy };
	VECTOR3 p = rot * 0.5f;
	position += p;
	target = front + position;
	camera.SetFocus(target);
}

void CameraControl::TPSCamera(float elapsedTime)
{
}
