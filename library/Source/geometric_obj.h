#pragma once

#include "geometric_primitive.h"

class GeometricObj
{
public:
	GeometricObj()	{}
	~GeometricObj() {}

	void Update()
	{
		DirectX::XMMATRIX S, R, T, W;
		S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
		R = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
		T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
		W = S * R * T;
		DirectX::XMStoreFloat4x4(&world, W);
	}
	void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color = VECTOR4(1.f, 1.f, 1.f, 1.f))
	{
		if (obj)
		{
			obj->SetCbMatrix(context, world, view, projection);
			obj->Render(context, view, projection, color);
		}
	}

	void SetScale(const VECTOR3& scale) { this->scale = scale; };
	void SetAngle(const VECTOR3& angle) { this->angle = angle; }
	void SetPosition(const VECTOR3& position) { this->position = position; }
	void SetGeometric(std::shared_ptr<GeometricPrimitive>& obj) { this->obj = obj; }

	const VECTOR3& GetScale() { return scale; }
	const VECTOR3& GetAngle() { return angle; }
	const VECTOR3& GetPosition() { return position; }

private:
	std::shared_ptr<GeometricPrimitive> obj = nullptr;

	VECTOR3 scale = VECTOR3(1, 1, 1);
	VECTOR3 angle = VECTOR3(0, 0, 0);
	VECTOR3 position = VECTOR3(0, 0, 0);
	FLOAT4X4 world = FLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

	//struct CBMatrix
	//{
	//	FLOAT4X4 world;
	//	FLOAT4X4 WVP;
	//};
	//std::unique_ptr<ConstantBuffer<CBMatrix>> cbMatrix;
	//void CBUpdate(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection)
	//{
	//	cbMatrix->data.world = world;
	//	DirectX::XMMATRIX w, v, p, wvp;
	//	w = DirectX::XMLoadFloat4x4(&world);
	//	v = DirectX::XMLoadFloat4x4(&view);
	//	p = DirectX::XMLoadFloat4x4(&projection);
	//	wvp = w * v * p;
	//	DirectX::XMStoreFloat4x4(&cbMatrix->data.WVP, wvp);

	//	cbMatrix->Activate(context, 1);
	//}
};