#pragma once

#include <d3d11.h>
#include <wrl.h>
#include <memory>
#include <vector>
#include "vector.h"
#include "constant_buffer.h"
#include "texture.h"
#include "shader.h"

using Microsoft::WRL::ComPtr;

class GeometricPrimitive
{
protected:
	struct Vertex
	{
		VECTOR3 position;
		VECTOR3 normal;
	};
	struct Mesh
	{
		ComPtr<ID3D11Buffer> vertexBuffer;
		int numV = 0;
		ComPtr<ID3D11Buffer> indexBuffer;
		int numI = 0;
	};
	Mesh mesh;
    
	struct CBColor
	{
		VECTOR4 materialColor;
	};
	std::unique_ptr<ConstantBuffer<CBColor>> cbColor;
	struct CBMatrix
	{
		FLOAT4X4 world;
		FLOAT4X4 WVP;
	};
	std::unique_ptr<ConstantBuffer<CBMatrix>> cbMatrix;

	std::unique_ptr<Shader> shader;

    bool	CreateBuffers(ID3D11Device* device,
        Vertex* vertices, int numV,
        unsigned int* indices, int numI);

protected:
    GeometricPrimitive()
    {}

public:
    GeometricPrimitive(ID3D11Device* device)
    {
		shader = std::make_unique<Shader>();
		shader->CreateGeometric(device);
    }
	virtual ~GeometricPrimitive() {}

	void SetCbMatrix(ID3D11DeviceContext* context, const FLOAT4X4&  world, const FLOAT4X4& view, const FLOAT4X4& projection);
	virtual void Render(ID3D11DeviceContext* context, const FLOAT4X4& view, const FLOAT4X4& projection, const VECTOR4& color = VECTOR4(1.f, 1.f, 1.f, 1.f));

};

//class GeometricRect : public GeometricPrimitive
//{
//public:
//	GeometricRect(ID3D11Device* device);
//	~GeometricRect() {};
//};


//class GeometricBoard : public GeometricPrimitive
//{
//public:
//	GeometricBoard(ID3D11Device* device);
//	GeometricBoard(ID3D11Device* device, int row, int col);
//	~GeometricBoard() {};
//};


class GeometricCube : public GeometricPrimitive
{
public:
	GeometricCube(ID3D11Device* device);
	~GeometricCube() {};
};


class GeometricSphere : public GeometricPrimitive
{
public:
	GeometricSphere(ID3D11Device* device, int slices = 16, int stacks = 16);
	~GeometricSphere() {};
};
