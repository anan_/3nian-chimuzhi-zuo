
#include "texture.h"
#include "create_DX11.h"
#include "resource_manager.h"
#include "WICTextureLoader.h"

using namespace GetCoreSystem;

bool Texture::Load(ID3D11Device* device, const wchar_t* filename)
{
	HRESULT hr = S_OK;

	ResourceManager::LoadShaderResourceView(device, filename, shaderResouceView.GetAddressOf(), &texture2dDesc);

	D3D11_SAMPLER_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sd.MinLOD = 0;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	hr = device->CreateSamplerState(
		&sd, samplerState.GetAddressOf());
	assert(SUCCEEDED(hr));
	//samplerState = std::make_unique<SamplerState>(device, 0);

	return true;
}

void Texture::Set(ID3D11DeviceContext* context, UINT Slot, BOOL flg)
{
	if (!flg)
	{
		ID3D11ShaderResourceView* srv[1] = { nullptr };
		ID3D11SamplerState* ss[1] = { nullptr };
		context->PSSetShaderResources(Slot, 1, srv);
		context->PSSetSamplers(Slot, 1, ss);
		context->DSSetShaderResources(Slot, 1, srv);
		context->DSSetSamplers(Slot, 1, ss);
		//samplerState->Activate(context, Slot);
		return;
	}
	if (shaderResouceView)
	{
		context->PSSetShaderResources(Slot, 1, shaderResouceView.GetAddressOf());
		context->PSSetSamplers(Slot, 1, samplerState.GetAddressOf());
		context->DSSetShaderResources(Slot, 1, shaderResouceView.GetAddressOf());
		context->DSSetSamplers(Slot, 1, samplerState.GetAddressOf());
	}
}

bool Texture::Create(ID3D11Device* device, u_int width, u_int height, DXGI_FORMAT format)
{
	ComPtr<ID3D11Texture2D> Texture2D;
	HRESULT hr = S_OK;
	//	テクスチャ作成
	ZeroMemory(&texture2dDesc, sizeof(texture2dDesc));
	texture2dDesc.Width = width;
	texture2dDesc.Height = height;
	texture2dDesc.MipLevels = 1;
	texture2dDesc.ArraySize = 1;
	texture2dDesc.Format = format;
	texture2dDesc.SampleDesc.Count = 1;
	texture2dDesc.Usage = D3D11_USAGE_DEFAULT;
	texture2dDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	texture2dDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

	hr = device->CreateTexture2D(&texture2dDesc, NULL, Texture2D.GetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}

	//	レンダーターゲットビュー作成
	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	ZeroMemory(&rtvd, sizeof(rtvd));
	rtvd.Format = format;
	rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvd.Texture2D.MipSlice = 0;
	hr = device->CreateRenderTargetView(Texture2D.Get(), &rtvd, renderTargetView.GetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}


	//	シェーダーリソースビュー作成
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	ZeroMemory(&srvd, sizeof(srvd));
	srvd.Format = format;
	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Texture2D.MostDetailedMip = 0;
	srvd.Texture2D.MipLevels = 1;
	hr = device->CreateShaderResourceView(Texture2D.Get(), &srvd, shaderResouceView.GetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}


	////	サンプラステート作成
	//D3D11_SAMPLER_DESC sd;
	//ZeroMemory(&sd, sizeof(sd));
	//sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	//sd.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	//sd.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	//sd.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	//sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
	////ボーダーカラー
	//sd.BorderColor[0] = 1.0f;
	//sd.BorderColor[1] = 1.0f;
	//sd.BorderColor[2] = 1.0f;
	//sd.BorderColor[3] = 1.0f;

	//sd.MinLOD = 0;
	//sd.MaxLOD = D3D11_FLOAT32_MAX;

	//hr = device->CreateSamplerState(&sd, samplerState.GetAddressOf());
	//if (FAILED(hr))
	//{
	//	return false;
	//}
	//samplerState = std::make_unique<SamplerState>(device, 2);

	return true;
}

