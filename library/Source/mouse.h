#pragma once

#include "create_DX11.h"
using namespace GetCoreSystem;
#include "sprite.h"
#include "input.h"

class Mouse
{
    static Mouse* instance;

private:
    POINT cursor, screenCursor = {};

    VECTOR2 prevPosition = {};
    VECTOR2 position = {};
    VECTOR2 screenCursorPosition = {};
    VECTOR2 scale = {};
    VECTOR2 speed = VECTOR2(0.5f, 0.5f);
    VECTOR2 move = {};

    //std::unique_ptr<Sprite> cursorImg = nullptr;


public:
    Mouse() { }
    ~Mouse() { }

    void Update();
    void Render();
    
    static bool LeftButtonState(TRIGGER_MODE mode);
    static bool RightButtonState(TRIGGER_MODE mode);
    static bool CenterButtonState(TRIGGER_MODE mode);

    void SetCursorImg(const wchar_t* filename);
    void SetScale(const VECTOR2& scale) { this->scale = scale; }
    void SetCursorPosition(const VECTOR2& position);
    void SetMove(const VECTOR2& move = VECTOR2(0, 0)) { this->move = move; }
    void SetSpeed(const VECTOR2& speed) { this->speed = speed; }

    void SetScreenCenter(float width, float heifgt);

    const VECTOR2& GetPosition() const { return position; }
    const VECTOR2& GetScreenPosition() const { return screenCursorPosition; }
    const VECTOR2& GetMove() const { return move; }

public:
    static void Create()
    {
        if (instance != nullptr) return;
        instance = new Mouse();
    }
    static Mouse& GetInstance()
    {
        return *instance;
    }
    static void Destory()
    {
        if (instance != nullptr)
        {
            delete instance;
            instance = nullptr;
        }
    }
};