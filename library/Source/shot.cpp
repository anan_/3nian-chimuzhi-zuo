#include "shot.h"
#include "shot_manager.h"

Shot::Shot(std::shared_ptr<GeometricPrimitive> geo)
{
    shot = std::make_shared<ShotObj>(geo);
    shot->SetScale(VECTOR3(2, 2, 2));
    shot->SetExist(false);
    pShotManager->ObjSet(shot);
}

void Shot::Update()
{
    shot->Update();
}

void Shot::Set(VECTOR3 pos, VECTOR3 angle)
{
    shot->SetPosition(VECTOR3(pos.x + sinf(angle.y) * 2, pos.y, pos.z + cosf(angle.y) * 2));
    shot->SetExist(true);
}
